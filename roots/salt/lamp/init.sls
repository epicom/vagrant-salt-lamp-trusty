apache2:
  pkg:
    - installed

mysql-server:
  pkg:
    - installed

php5:
  pkg:
    - installed

php-pear:
  pkg:
    - installed

php5-mysql:
  pkg:
    - installed

php5-curl:
  pkg:
    - installed

/etc/apache2/httpd.conf:
   file:
     - managed
     - user: root
     - group: root
     - mode: 644
     - source: salt://lamp/httpd.conf
