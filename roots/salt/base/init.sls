install required packages:
  pkg.installed:
    - names:
      - ubuntu-standard
      - build-essential
      - python-pip
      - dnsutils
      - sysstat
      - postfix
      - openssl
      - screen
      - unzip
      - rsync
      - curl
      - ntp
      - vim
      - bc

configure /etc/timezone:
  file.managed:
    - name: "/etc/timezone"
    - contents: "Etc/UTC"

dpkg-reconfigure --frontend noninteractive tzdata:
  cmd.run

set language:
  cmd.run:
    - name: "/usr/sbin/locale-gen en_US.UTF-8 && /usr/sbin/update-locale LANG=en_US.UTF-8"

# create uploads directory:
#   file.directory:
#     - name: /srv/www/public/uploads
#     - mode: 755
#     - user: vagrant
#     - group: vagrant
#     - makedirs: true
