/etc/update-motd.d/98-custom:
   file:
     - managed
     - template: jinja
     - user: root
     - group: root
     - mode: 755
     - source: salt://motd/motd.jinja2
